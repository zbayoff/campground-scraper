const { checkPermits } = require("./handler");

describe("No available permits", () => {
  it("should return an empty array", () => {
    const noAvailablePermitResponse = {
      data: {
        payload: {
          permit_id: "72192",
          next_available_date: "2021-04-30T00:00:00Z",
          availability: {
            113414: {
              division_id: "113414",
              date_availability: {
                "2021-04-01T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-02T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-03T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-04T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-05T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-06T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-07T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-08T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-09T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-10T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-11T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-12T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-13T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-14T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-15T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-16T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-17T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-18T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-19T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-20T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-21T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-22T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-23T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-24T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-25T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-26T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-27T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-28T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-29T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-30T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
              },
              quota_type_maps: {
                QuotaUsageByMember: {
                  "2021-04-01T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-02T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-03T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-04T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-05T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-06T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-07T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-08T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-09T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-10T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-11T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-12T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-13T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-14T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-15T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-16T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-17T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-18T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-19T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-20T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-21T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-22T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-23T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-24T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-25T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-26T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-27T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-28T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-29T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-30T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                },
              },
            },
          },
        },
      },
    };

    const expected = [];
    const permits = checkPermits(noAvailablePermitResponse);
    expect(permits).toEqual(expected);
  });
});

describe("Available permits", () => {
  it("should return the availability array in between startDate and endDate", () => {
    const expectedAvailability = [
      {
        date: "2021-04-26T00:00:00Z",
        is_secret_quota: false,
        remaining: 2,
        show_walkup: false,
        total: 30,
      },
      {
        date: "2021-04-27T00:00:00Z",
        is_secret_quota: false,
        remaining: 2,
        show_walkup: false,
        total: 30,
      },
      {
        date: "2021-04-28T00:00:00Z",
        is_secret_quota: false,
        remaining: 4,
        show_walkup: false,
        total: 30,
      },
    ];

    const availablePermitResponse = {
      data: {
        payload: {
          permit_id: "72192",
          next_available_date: "2021-04-30T00:00:00Z",
          availability: {
            113414: {
              division_id: "113414",
              date_availability: {
                "2021-04-01T00:00:00Z": {
                  total: 30,
                  remaining: 5,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-02T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-03T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-04T00:00:00Z": {
                  total: 30,
                  remaining: 2,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-05T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-06T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-07T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-08T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-09T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-10T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-11T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-12T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-13T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-14T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-15T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-16T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-17T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-18T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-19T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-20T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-21T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-22T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-23T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-24T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-25T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-26T00:00:00Z": {
                  total: 30,
                  remaining: 2,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-27T00:00:00Z": {
                  total: 30,
                  remaining: 2,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-28T00:00:00Z": {
                  total: 30,
                  remaining: 4,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-29T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
                "2021-04-30T00:00:00Z": {
                  total: 30,
                  remaining: 0,
                  show_walkup: false,
                  is_secret_quota: false,
                },
              },
              quota_type_maps: {
                QuotaUsageByMember: {
                  "2021-04-01T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-02T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-03T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-04T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-05T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-06T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-07T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-08T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-09T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-10T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-11T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-12T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-13T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-14T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-15T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-16T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-17T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-18T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-19T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-20T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-21T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-22T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-23T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-24T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-25T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-26T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-27T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-28T00:00:00Z": {
                    total: 30,
                    remaining: 4,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-29T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                  "2021-04-30T00:00:00Z": {
                    total: 30,
                    remaining: 0,
                    show_walkup: false,
                    is_secret_quota: false,
                  },
                },
              },
            },
          },
        },
      },
    };

    const expected = expectedAvailability;
    const permits = checkPermits(availablePermitResponse, "2021-04-23", "2021-04-28");
    expect(permits).toEqual(expected);
  });
});

// describe("Available campsites", () => {
//   it("should return the availability array with a new date property", () => {
//     const expected = expectedAvailability;
//     const permits = checkPermits(availablePermitResponse);
//     expect(permits).toEqual(expected);
//   })
// })
